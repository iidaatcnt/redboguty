# coding: utf-8

def pochi():
  D_COINS = 5
  C_COINS = 0
  L_COINS = 0
  X_COINS = 1
  V_COINS = 1
  I_COINS = 2
  POCHI   = 2017
  cnt = 0
  for d in xrange(D_COINS+1):
    if POCHI < d*500 : break
    for c in xrange(C_COINS+1):
      if POCHI < d*500 + c*100 : break
      for l in xrange(L_COINS+1):
        if POCHI < d*500 + c*100 + l*50 : break
        for x in xrange(X_COINS+1):
          if POCHI < d*500 + c*100 + l*50 + x*10 : break
          for v in xrange(V_COINS+1):
            s = POCHI - (d*500 + c*100 + l*50 + x*10+ v*5)
            if 0 < s <= I_COINS :
              # print "%d,%d,%d,%d,%d,%d" % (d,c,l,x,v,I_COINS)
              cnt += 1
  print "%d\n" % cnt

if __name__ == '__main__':
  pochi()
