#!/bin/bash
#
# .pyで終わるファイルの名前を.py_yymmddにする

for FN in *.py
do
  LOG="${FN%py}py_$(date '+%Y%m%d').log"
  TFN="${FN%py}py_$(date '+%Y%m%d')"
  mv ${FN} ${TFN}
  date > ${TFN}
  mv ${TFN} ${FN} 
done
