#!/bin/bash
#
# .phpで終わるファイルの名前を.php_yymmddにする

for FN in *.php
do
  mv "${FN}" "${FN%php}php_$(date '+%Y%m%d')"
done
